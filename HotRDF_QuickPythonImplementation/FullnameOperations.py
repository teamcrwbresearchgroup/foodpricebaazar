
MakeProperty = lambda field,string: field + string

CreateSnippet = lambda time,price,properties: Snippet(time,price,properties)

AddProperty = lambda snippet, property: snippet < property

IsCompatible = lambda snippetOrFeed, snippet: snippetOrFeed / snippet

MakeFeed = lambda snippet1, snippet2: snippet1 + snippet2

#IsCompatible = lambda snippetOrFeed, snippet: snippetOrFeed / snippet

AddSnippet = lambda feed, snippet : feed < snippet

GetPoint = lambda ts, integer: GetTSM(ts).ReconstructPoint(ts,integer)

PredictPoint = lambda ts, integer: GetTSM(ts).PredictPoint(ts,integer)

GetPoints = lambda ts: GetTSM(ts).GetAvailablePoints(ts)

Apply = lambda model,sfeed: model.ConsumeFeed(sfeed)

ComposeApply = lambda compose, model, pfeed: Apply(model, compose(pfeed))