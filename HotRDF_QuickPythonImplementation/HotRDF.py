from FullnameOperations import *

# Empty definitions for type hints
class Field():
    pass
class Property():
    pass
class Snippet():
    pass
class SFeed():
    pass

class Field(str):
    # MakeProperty (+)
    def __add__(self, other: str) -> Property:
        assert (other is str)
        return Property(self, other)

class Property():
    def __init__(self, field: Field, value: str) -> None:
        assert(type(field) is Field)
        self.field = field
        self.value = value

class SnippetBody():
    pass

class SnippetCluster(SnippetBody):
    def __init__(self):
        self.list = []

class Snippet(SnippetBody):
    # CreateSnippet
    def __init__(self, time:int, price:int, **properties:list) -> None:
        self.time = time
        self.price = price
        self.values = properties

    # AddProperty (<)
    def __lt__(self, other:Property) -> Snippet:
        assert(type(other) is Property)
        assert(other.field not in self.values.keys())
        self.values[other.field] = other.value
        return self

    # AddProperty (<)
    def __lt__property(self, other:Property) -> Snippet:
        assert(type(other) is Property)
        assert(other.field not in self.values.keys())
        self.values[other.field] = other.value
        return self

    # To facilitate the use of "<"
    def __lt__snippet(self, other:SnippetBody) -> SnippetCluster:
        othertype = type(other)
        assert ( othertype is Snippet or othertype is SnippetCluster)
        cluster = SnippetCluster()
        if(othertype is Snippet):
            cluster.list += other
        elif(othertype is SnippetCluster):
            cluster.list = other.list
        cluster.list += self
        return cluster

    # MakeFeed (+)
    def __add__(self, other:Snippet) -> SFeed:
        assert (type (other) is Snippet)
        return SFeed(self, other)

    # IsCompatible (/)
    def __truediv__(self, other:Snippet) -> bool:
        assert(type(other) is Snippet)
        return checkCompatibility(self, other)


class Feed():
    pass

class SFeed(Feed):
    def __init__(self, *snippets:list)-> None:
        self.snippets = snippets

    # IsCompatible (/)
    def __truediv__(self, other:Snippet) -> bool:
        assert(type(other) is Snippet)
        compatible = True
        for snip in self.snippets:
            if not checkCompatibility(snip, other):
                compatible = False
                break
        return compatible

    # AddSnippet (<)
    def __lt__(self, other:Snippet) -> SFeed:
        assert(type(other) is Snippet)
        assert(self / other)
        self.snippets += other
        return self


LastTs = 0

class TS():
    def __init__(self, data, id = -1):
        if id == -1:
            global LastTs
            id = LastTs
            LastTs +=1
        self.id = id
        self.data = data

class Model():
    def ConsumeFeed(self,feed):
        pass

    def ReconstructPoint(self, ts, integer):
        pass

    def PredictPoint(self, ts, integer):
        pass

    def GetAvailablePoints(self):
        pass